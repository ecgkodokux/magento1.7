magento1.7は、何もしていない奇麗なmagentoバージョン1.7.0.2です！
=================================================

magentoの新規プロジェクトの開始に使って頂ければ、楽のではないかなと思いで
このリポジトリを作ってあります。



プロジェクトの準備
---------------------------------------------

Gitでmagentoをcloneします。

    $ git clone git@bitbucket.org:ecgkodokux/magento1.7.git public

##magerunをインストール (簡単にパッケージなどを入れる為に)

    $ curl -o n98-magerun.phar https://raw.github.com/netz98/n98-magerun/master/n98-magerun.phar
    $ chmod +x ./n98-magerun.phar
    $ sudo cp ./n98-magerun.phar /usr/local/bin/

local.xmlの設定をして下さい。

###demoデーターの流し込み (magentoのデモデーター)
    $ cd /path/to/magento
    $ mysql -u hogehoge -p < dataloper.sql

core_config_dataテーブルの, web/unsecure/base_url と web/secure/base_url を自分のホスト名に変更して下さい。


debian の場合は php.iniでpharを実行できるようにして下さい。

    suhosin.executor.include.whitelist="phar"

###magerunのアップデート (必要な場合のみ)
    $ n98-magerun.phar self-update


###Magerunを使って、日本語パッケージをインストール
    $ cd /path/to/mangeto
    $ sudo n98-magerun.phar extension:install ECGiken_Japanese

##modmanのインストール (他のエクステンションのリポジトリのシンボリックを張ってくれる！)
    $ bash < <(curl -s https://raw.github.com/colinmollenhour/modman/master/modman-installer)
    $ source ~/.profile

###modmanの準備
    $ cd /path/to/mangeto
    $ ~/bin/modman init

.modmanのディレクトリが作成されたら、成功です！

###kodokux-debug のインストール (modmanの使用例です)

これで、kodokux-debugのエクステンションがmagento直下の.modmanにcloneされて、
modmanファイルのマッピングそって、magentoのツリーに正しくシンボリックリンクが張られる！

    $ cd /path/to/mangeto
    $ ~/bin/modman https://github.com/Kodokux/kodokux-debug.git

